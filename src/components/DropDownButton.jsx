import React, { useState, useRef, useEffect } from "react";
import "../assets/styles/dropdownbutton.css";
import FontAwesome from "react-fontawesome";

// Dummy Data
const dummyData = [
  { label: "Apple", value: 1, check: false },
  { label: "Facebook", value: 2, check: false },
  { label: "Netflix", value: 3, check: false },
  { label: "Tesla", value: 4, check: false },
  { label: "Amazon", value: 5, check: false },
  { label: "Alphabet", value: 6, check: false },
];

const DropDownButton = () => {
  const ref = useRef();
  const [options, setOptions] = useState(dummyData);
  const [listOpen, setlistOpen] = useState(false);
  const [selectId, setselectId] = useState(null);
  const [selectLabel, setselectLabel] = useState("Select Option");

//   Close Dropdown on Outside Click
  useEffect(() => {
    const checkIfClickedOutside = (e) => {
       if (listOpen && ref.current && !ref.current.contains(e.target)) {
        setlistOpen(false);
      }
    };

    document.addEventListener("mousedown", checkIfClickedOutside);

    return () => {
      // Cleanup the event listener
      document.removeEventListener("mousedown", checkIfClickedOutside);
    };
  }, [listOpen]);

  
  // Update Function
  const selectFun = (dt) => {
    setselectId(dt.value);
    setselectLabel(dt.label);

    setOptions(
      options.map((obj) =>
        obj.label === dt.label
          ? { ...obj, check: true }
          : { ...obj, check: false }
      )
    );
  };

  return (
    <div className="dropdown-input" ref={ref}>
      <div className="dropdown-btn" onClick={()=>{
          setlistOpen(prev=>!prev)
      }}>
        {selectLabel}
        
        {listOpen ? (
          <FontAwesome name="caret-down" />
        ) : (
          <FontAwesome name="caret-left" />
        )}
        {listOpen && (
          <div className="dropdown-items">
            {options.map((dt) => (
              <div
                className="dropdown-item"
                onClick={(e) => selectFun(dt)}
                key={dt.value}
              >
                {dt.label} {dt.check && <FontAwesome name="check" />}
              </div>
            ))}
          </div>
        )}
      </div>
      {!selectId && <div id="error-message">Required*</div>}
    </div>
  );
};

export default DropDownButton;
